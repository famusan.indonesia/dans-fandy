# Dans Multi Pro Mini Project

## Reqiured tools for run this apps
* Mysql
* Java min 8 version
* Postman as API testing tool

## How to use?
1. Create db with your mysql
   > CREATE DATABASE `dans`
2. Update db config in application.properties using your db
3. Run the code
4. Register (create) user
6. Authenticate (login) user for get token
7. Use the token for using api service
8. Congratulation you ready to use this api
9. Please import json postman collection in this project "dans multi pro.postman_collection.json" for more documentation or you can use this link
   > https://restless-satellite-660959.postman.co/workspace/jwt~50a1ab95-a2e1-47ca-b874-cc2e021a0847/collection/1476537-ef39af49-b928-4f38-84ec-ebc784087fac?action=share&creator=1476537
   

