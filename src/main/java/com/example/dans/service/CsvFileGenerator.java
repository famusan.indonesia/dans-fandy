package com.example.dans.service;

import com.example.dans.entity.Job;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Component
public class CsvFileGenerator {
    public void writeStudentsToCsv(List<Job> jobs, Writer writer) {
        try {

            CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);
            for (Job job : jobs) {
                printer.printRecord(job.getId(),
                        job.getType(),
                        job.getUrl(),
                        job.getCompany(),
                        job.getCompanyUrl(),
                        job.getLocation(),
                        job.getTitle(),
                        job.getDescription(),
                        job.getHowToApply(),
                        job.getCompanyLogo(),
                        job.getCreated()
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
