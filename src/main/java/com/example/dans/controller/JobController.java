package com.example.dans.controller;

import com.example.dans.entity.Job;
import com.example.dans.repository.JobRepository;
import com.example.dans.service.CsvFileGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/Job")
public class JobController {
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private CsvFileGenerator csvFileGenerator;


    @GetMapping({"/", ""})
    public ResponseEntity<?> getJobs() {
        try {
            List<Job> jobs = jobRepository.findAll();
            if (jobs.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(jobs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping({"/", ""})
    public ResponseEntity<?> postJobs(@RequestBody List<Job> jobs) {
        try {
            jobs = jobRepository.saveAll(jobs);
            return new ResponseEntity<>(jobs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping({"/{id}", "/{id}/"})
    public ResponseEntity<?> getJob(@PathVariable Long id) {
        try {
            Job job = jobRepository.findById(id).orElse(null);
            if (null == job)
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                return new ResponseEntity<>(job, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping({"/", ""})
    public ResponseEntity<?> putJobs(@RequestBody Job job) {
        try {
            job = jobRepository.save(job);
            return new ResponseEntity<>(job, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping({"/", ""})
    public ResponseEntity<HttpStatus> deleteJob(@RequestBody Job job) {
        try {
            jobRepository.delete(job);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping({"/download-jobs", "/download-jobs/"})
    public void downloadJob(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.addHeader("Content-Disposition", "attachment; filename=\"jobs.csv\"");
        csvFileGenerator.writeStudentsToCsv(jobRepository.findAll(), response.getWriter());
    }
}
